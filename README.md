> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**


# Course LIS3781 Advanced Database Management

## Yanheng Chen 


### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Made and forward engineered ERD
    - Done basic SQL statements for the forward engineered database
    - Course Installations
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Created and scripted two tables using SQL
    - Granted created to two user in local server
    - Managed and tested permission to the two users created as admin
    - Deleted the tables in local server as admin
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Used Oracle to generate database structures 
    - Used Oracle to populate tables within generated database
    - Did required SQL reports using data within database created

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Used Microsoft SQL server to develop relational database using T-SQL
    - Created ERD using MS SQL Server
    - Generated required reports using T-SQL

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Used Microsoft SQL server to develop data store
    - Created ERD using MS SQL Server
    - Generated required reports using T-SQL

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Created relational database following the business rules
    - Generated required reports using SQL
    - Generated SQL statements to create a certain amount of automation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Development installation for MongoDB
    - Generated required reports using JSON on MongoDB
    - Learned about different commands of MongoDB