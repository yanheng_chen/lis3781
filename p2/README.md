> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database

## Yanheng Chen

### Project 5 Requirements:

*Three Parts:*

1. Development installation of Mongodb
2. Imported dataset of to Mongodb
3. Worked on queries on the imported Mongodb dataset




#### Assignment Screenshots:

*Mongodb Installations*
![table](img/mongoserver.png) 
![table](img/mongoshell.png)

*Command used in mongodb*
![A5 SQL File](img/all_res.png)
[All of the commands used](answer.js)



