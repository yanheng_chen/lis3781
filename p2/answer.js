//1. 
db.restaurants.find() //Display all documents in collection. 
//2.
db.restaurants.find().count() // display number of documents in the collection(restaturant)
//3
db.restaurants.find().count().limit(5) //Retrieve 1st 5 documents.
//4
db.restaurants.find( { "borough" : "Brooklyn" } ) //Retrieve restaurants in the Brooklyn borough.
//5
db.restaurants.find({"cuisine": "American "}) //Retrieve restaurants whose cuisine is American
db.restaurants.find({"cuisine": "/^American\s$"}) //regex that includes white space characters
db.restaurants.find({"cuisine": "/^american\s$/i"}) //regex that includes white space characters and is case insensitive
//6
db.restaurants.find( { "borough" : "Manhattan", "cuisine": "Hamburgers" } ) //Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers. 
//7
db.restaurants.find( { "borough" : "Manhattan", "cuisine": "Hamburgers" } ).count() ////Retrieve the number of restaurants whose borough is Manhattan and cuisine is hamburgers
//8
db.restaurants.find({"address.zipcode": "10075"}) //Query zipcode field in embedded address document. Retrieve restaurants in the 10075 zip code area. 
//the "address.zipcode" references the zipcode field that is embeded into the address field
//9
db.restaurants.find({"cuisine": "Chicken", "address.zipcode": "10024"}) //Retrieve restaurants whose cuisine is chicken and zip code is 10024. 
        //this syntax is implicitly implying that it is an AND operator
        db.restaurants.find({ $and: [{ "cuisine": "Chicken"}, {"address.zipcode": "10024"}]})  
        //                      ^    specifies the or operator
//10
db.restaurants.find({ $or: [{ "cuisine": "Chicken"}, {"address.zipcode": "10024"}]}) //Retrieve restaurants whose cuisine is chicken or zip code is 10024.
//                      ^    specifies the or operator
//11
db.restaurants.find({"borough": "Queens", "cuisine" : "Jewish/Kosher"}).sort({"address.zipcode":-1}) //Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sort by descending order of zipcode. 
//                                                                                                 ^ indicated sorting by descending order of zipcode field
//12
db.restaurants.find({"grades.grade" : "A"}) // Retrieve restaurants with a grade A. 
db.restaurants.find({"grades.grade" : "A"}).limit(1).pretty()
//13
db.restaurants.find({"grades.grade" : "A"}, {"name": 1, "grades.grade": 1}) // Retrieve restaurants with a grade A. Retrieve restaurants with a grade A, displaying only collection id, restaurant name, and grade. 
//                                                   ^                  ^ these two ones tells MongoDB with fields to show for the result set along with the grade field
//Note: restaurants can have more than one grade and/or score
//14
db.restaurants.find({"grades.grade" : "A"}, {"name": 1, "grades.grade": 1, _id:0}) //Retrieve restaurants with a grade A, displaying only restaurant name, and grade (no collection id): 
//                                                   ^                  ^ these two ones tells MongoDB with fields to show for the result set along with the grade field
//                                                                            ^ the _id:0 tells MongoDB to not show the collection ID of the record
//15
db.restaurants.find({"grades.grade" : "A"}).sort({"cuisine":1, "address.zipcode":-1}) //Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending. 
//                                                          ^ this 1 means sort the cuisine field by ascending order
//                                                                                ^this -1 means sort the zipcode embeded in the address field by decending order
//16
db.restaurants.find({"grades.score": {$gt: 80 }}) // Retrieve restaurants with a score higher than 80. 
//                                      ^$gt is an intrinsic operation(kind of like a function) in MongoDB that allows users to see get numbers greater than the indicated number

//17
/*
Insert a record with the following data: 
street = 7th Avenue 
zip code = 10024 
building = 1000 
coord = -58.9557413, 31.7720266 
borough = Brooklyn 
cuisine = BBQ 
date = 2015-11-05T00:00:00Z 
grade" : C 
score = 15 
name = Big Tex 
restaurant_id = 61704627 
*/
//check records before and after inserts
db.restaurants.insert({
    "address" : {
        "street" : "7th Avenue",
        "zipcode" : "10024",
        "building" : "1000",
        "coord" : [-58.9557413, 31.7720266],
    },
    "borough" : "Brooklyn",
    "cuisine" : "BBQ",
    "grades" : [{
        "date" : ISODate("2015-11-05T00:00:00Z"),
        "grade" : "C",
        "score" : 15
    }],
    "name" : "Big Tex",
    "restaurant_id" : "61704627"
    }   
    )
//MongoDB server will automatically give us the ID for each record inserted

//18
/*
Update the following record: 
Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the 
lastModified field with the current date. 
*/
//after finding out what record we need to update
db.restaurants.update(
    {
        "_id" : ObjectId("5e9ca93998816875e84c47ca")
    },
    {
        $set: {"cuisine" : "Steak and Sea Food"}, 
        $currentDate: {"lastModified" : true}
    }
)

//19
/*
Delete the following records: 
Delete all White Castle restaurants.
*/
db.restaurants.remove({"name" : "White Castle"});