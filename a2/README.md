> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Development

## Yanheng Chen

### Assignment 2 Requirements:

*Three Parts:*

1. Manually created and linked two tables locally
2. Making users and granted them privileges
3. Dropped the tables created





#### Assignment Screenshots:

*Screenshot of table creation SQL statements *:

![Table SQL Queries](img/table_creation_1.png "screenshot of SQL that creates and links the two tables")

![Table SQL Queries](img/table_creation_2.png "screenshot of SQL that creates and links the two tables")

![Table SQL Queries](img/table_creation_3.png "screenshot of SQL that creates and links the two tables")

![Table SQL Queries](img/table_creation_4.png "screenshot of SQL that creates and links the two tables")


*Screenshot of table contents*

![Table contents POV: user1](img/see_data_company_user1.png "screenshot of contents from company table")

![Table contents POV: user2](img/see_data_customer_user2.png "screenshot of contents from customer table")


*Screenshot of creating user1 and user2 in local database*:

![Create user1 in local database](img/user_1_creation.png "Creates user1")

![Create user2 in local database](img/user_2_creation.png "Creates user2")



*Screenshot of granting privileges for user1 and user2 in local database*:

![Grant privileges for user1 in local database](img/user_1_grants.png "Creates user1")

![Grant privileges for user2 in local database](img/user_1_grants.png "Creates user1")



*Screenshot of database and table permissions*:

![Permissions for user1](img/user1_privileges.png "Permissions for user1")

![Permissions for user2](img/user2_privileges.png "Permissions for user2")

![Permissions for admin](img/admin_privileges.png "Permissions for admin")


*Screenshot of current user2 logged in view*:

![Current user2](img/current_user2.png "Current user2")


*Screenshot of listed tables in admin POV*:

![Tables in database(Admin POV)](img/listed_tables.png "Tables in database from POV of admin user")


*Screenshot of table structures in admin POV*:

![Tables structures for company and customer tables](img/table_structures.png "Tables structures for company and customer tables")


*Screenshot of permission denied for user1*:

![Permission denied for inserts for user1](img/insert_denied_user1.png "Permission denied for inserts for user1")


*Screenshot of permission denied for user2*:

![Permission denied for inserts for user2](img/permission_denied_user2.png "Permission denied for inserts for user2")

![Permission denied for inserts for user2](img/permission_denied_user2_company.png "Permission denied for inserts for user2")


*Screenshot of delete tables from admin*:

![Deleting both tables as admin](img/delete_tables.png "Deleting both tables as admin")




