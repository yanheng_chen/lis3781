drop database if exists yc19a;
create database if not exists yc19a;
use yc19a;


-- Table company
 
DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip int(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
cmp_email VARCHAR(100) NULL,
cmp_url VARCHAR(100) NULL,
cmp_notes VARCHAR(255) NULL,
PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null, 'LLC', '3305 Memory Lane','Sterling', 'IL', '256301842', '2014755510', '986538.00', null, 'http://www.alasenyorita.com','company notes1'),
(null, 'C-Corp', '3761 Elk Street','Newport Beach', 'CA', '996141842', '1238956510', '778678.00', null, 'http://www.dajujor.com','company notes2'),
(null, 'S-Corp', '2407 Arlington Avenue','Paragould', 'AR', '725601842', '9865745510', '569678.00', null, 'http://www.qrifi.com','company notes3'),
(null, 'Non-Profit-Corp', '2425 Clinton Street','Little Rock', 'AR', '659871842', '3625455510', '123678.00', null, 'http://www.babybread.com','company notes4'),
(null, 'Partnership', '4726 - 11th Ave. N.E.','Seattle', 'WA', '001501842', '2720655510', '345678.00', null, 'http://www.qualityinstruction','company notes5');

SHOW WARNINGS;

select * from company;

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_id INT UNSIGNED NOT NULL,
cus_ssn binary(64) not null,
cus_salt binary(64) not null COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!',
cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
cus_first VARCHAR(15) NOT NULL,
cus_last VARCHAR(30) NOT NULL,
cus_street VARCHAR(30) NULL,
cus_city VARCHAR(30) NULL,
cus_state CHAR(2) NULL,
cus_zip int(9) unsigned ZEROFILL NULL,
cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7,2) unsigned NULL COMMENT '12,345.67',
cus_tot_sales DECIMAL(7,2) unsigned NULL,
cus_notes VARCHAR(255) NULL,
PRIMARY KEY (cus_id),
UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
INDEX idx_cmp_id (cmp_id ASC),

CONSTRAINT fk_customer_company
FOREIGN KEY (cmp_id)
REFERENCES company (cmp_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO customer
 VALUES

(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso', 'TX', '085703412', '7214555985', 'testl@mymaiLcom', '8391.87', '73764.20', 'custonner notes1'),
(null,4,unhex(SHA2(CONCAT(@salt, 000145236),512)),@salt,'Loyal','Linda','Heaney','567 Public Works Drive','Chattanooga', 'TN', '37895412', '9825635985', 'LindaJHeaney@jourrapide.com', '8991.87', '98664.20', 'custonner notes2'),
(null,3,unhex(SHA2(CONCAT(@salt, 000896523),512)),@salt,'Impulse','Lorie','Barnes','4379 Tori Lane','West Valley City', 'TX', '456323412', '7896555985', '
LorieWBarnes@teleworm.us', '9875.63', '44444.20', 'custonner notes3'),
(null,5,unhex(SHA2(CONCAT(@salt, 000451154),512)),@salt,'Need-Based','Beatrice','Roberts','3775 Cambridge Place','Baltimore', 'MD', '659874512', '0212165985', 'BeatriceGRoberts@rhyta.com', '9391.87', '73764.20', 'custonner notes4'),
(null,1,unhex(SHA2(CONCAT(@salt, 000741258),512)),@salt,'Wandering','Ruth','English','4031 Edwards Street','Greenville', 'NC', '987403412', '3625455985', 'RuthGEnglish@dayrep.com', '8391.87', '73764.20', 'custonner notes5');


SHOW WARNINGS;
-- salting and hashing sensitive data (e.g., S$ B). Normally, *each* record would receive unique random salt! 
 set @salt=RANDOM_BYTES(64); 

select * from company;
select * from customer;

show grants for 'root'@'localhost';