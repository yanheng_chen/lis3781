> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Databases

## Yanheng Chen

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. ERD development and forward engineering
4. SQL statements


#### README.md file should include the following items:




>
> #### Git commands w/short descriptions:

1. git init - initialize the repo locally
2. git status - describes the state of the current working directory and the staging area
3. git add - tells git to include update in the next commit
4. git commit - create a new commit with the current contents and giving a message describing the changes
5. git push - upload files from local to remote repo
6. git pull - download files from remote to local repo
7. git revert - reverts the action of the last command

## Business Rules for ERD
> The human resource (HR) department of the ACME company wants to contract a database
modeler/designer to collect the following employee data for tax purposes: job description, length of
employment, benefits, number of dependents and their relationships, DOB of both the employee and any
respective dependents. In addition, employees’ histories must be tracked. Also, include the following
business rules:

* Each employee may have one or more dependents.
* Each employee has only one job.
* Each job can be held by many employees.
* Many employees may receive many benefits.
* Many benefits may be selected by many employees (though, while they may not select any benefits—
any dependents of employees may be on an employee’s plan).

#### Additional information from business rules

* Employee: SSN, DOB, start/end dates, salary;
* Dependent: same information as their associated employee (though, not start/end dates), date added
(as dependent), type of relationship: e.g., father, mother, etc.
* Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
* Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
* Plan: type (single, spouse, family), cost, election date (plans must be unique)
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
* Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
* *All* tables must include notes attribute.


#### Assignment Screenshots:

* Screenshot of Ampps working:

![AMPPS working](img//ampss_working.png "screenshot of a1 ERD")

*Screenshot of ERD:

![a1_ERD](img/ERD.png "screenshot of a1 ERD")

*Screenshot of SQL code 1 and requirements*:

![a1 SQL query requirements](img/query_requirement_1.png)

![a1_SQL_1](img/SQL_query_for_question_1.png)

*Screenshot of SQL code 2 and requirements*:

![a1 SQL query requirements](img/query_requirement_2.png)

![a1_SQL_2](img/SQL_query_for_question_2.png)

*Screenshot of SQL code 3 and requirements*:

![a1 SQL query requirements](img/query_requirement_3.png)

![a1_SQL_3](img/SQL_query_for_question_3.png)

*Screenshot of SQL code 4 and requirements*:

![a1 SQL query requirements](img/query_requirement_4.png)

![a1_SQL_4](img/SQL_query_for_question_4.png)

*Screenshot of SQL code 5 and requirements*:

![a1 SQL query requirements](img/query_requirement_5.png)

![a1_SQL_5](img/SQL_query_for_question_5.png)

*Screenshot of SQL code 6 and requirements*:

![a1 SQL query requirements](img/query_requirement_6.png)

![a1_SQL_6](img/SQL_query_for_question_6.png)

*Screenshot of SQL code 7 and requirements*:

![a1 SQL query requirements](img/query_requirement_7.png)

![a1_SQL_6](img/SQL_query_for_question_7.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/yanheng_chen/bitbucketstationlocations/ "Bitbucket Station Locations")

