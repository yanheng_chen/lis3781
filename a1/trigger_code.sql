delimiter //
# no need to change delimiter if you are making the trigger on your ERD

create trigger trg_employee_after_insert
AFTER INSERT on employee
# after insert means that this trigger triggers after something is inserted on the employee table
FOR EACH ROW
BEGIN
INSERT into emp_hist
(emp_id, eht_date, eht_type, eht_job_id, eht_emp_salary, eht_user_changed, eht_reason, eht_notes)
values
# The NEW. keyword is used to indicate the newly inserted values on employee table
# The OLD. Keyword is used to indicate the data that used to be there on the employee table
(NEW.emp_id, now(), 'i', NEW.job_id, NEW.emp_salary, user(), "new employee", NEW.emp_notes);
# The user() function gets the user that is using the database to make changes at the time of change
# The now() function gets the current datetime of the time of change
END//

delimiter ;

