> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database

## Yanheng Chen

### Assignment 4 Requirements:

*Four Parts:*

1. Crafted database in Microsoft SQL Server using T-SQL
2. Created ERD with MS SQL Server
3. Worked on the queries required for the assignment reports
4. Created stored procedures for salting and hashing values for sensitive information (SSN)


> #### Business Rules:
* A sales representative has at least one customer, and each customer has at least one sales rep on any given day (as it is a high-volume organization). 
* A customer places at least one order. However, each order is placed by only one customer. 
* Each order contains at least one order line. Conversely, each order line is contained in exactly one order. 
* Each product may be on a number of order lines. Though, each order line contains exactly one product id (though, each product id may have a quantity of more than one included, e.g., “oln_qty”). 
* Each order is billed on one invoice, and each invoice is a bill for exactly one order (by only one 
customer). 
* An invoice can have one (full), or can have many payments (partial). Though, each payment is made to only one invoice. 
* A store has many invoices, but each invoice is associated with only one store. 
* A vendor provides many products, but each product is provided by only one vendor. 
* Must track yearly history of sales reps, including (also, see Entity-specific attributes below): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents). 
* Must track history of products, including: cost, price, and discount percentage (if any). 

> #### Additional Notes:
* A customer’s contact (in-store or online) is made through a sales rep. 
* A customer buys or potentially buys products from the company, but does not have to. 
* An order is a purchase of one or more products by a customer. If an order is cancelled, it is deleted (optional participation). 
* An order line contains the details about each product sold on a particular customer order, and includes data such as quantity and price. 
* A product is an item that the company sells that was initially bought from an outside vendor (which may also be the manufacturer). 
* A sales rep receives a 3% commission based upon the amount of year-to-date sales. 
* A sales reps’s current yearly sales goal is 8% more than their previous year’s total sales. 

#### Assignment Screenshots:

*ERD for A4 Database*
![table](img/erd.png) 
![table](img/erd2.png)


[A4 SQL File](A4_SQL.sql)

