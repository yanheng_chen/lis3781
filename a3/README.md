> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database 

## Yanheng Chen

### Assignment 3 Requirements:

*Three Parts:*

1. Created tables using Oracle database
2. Generated SQL statements for database design and table inserts
3. SQL statements to answer questions



#### Assignment Screenshots:


|       Populated databases     |             Table generating query 
| ----------------------------- | ------------------------------ |
|     ![table](img/tables.png)     | ![Table generating sql](img/sql.png) |
||  |
|   ![table](img/tables2.png)  | ![Table generating sql](img/sql2.png) |
||  |
|   ![table](img/tables3.png)  | ![Table generating sql](img/sql3.png) |
||  |
|                               | ![Table generating sql](img/sql4.png) |


