> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Adavanced Database Concepts

## Yanheng Chen

### Project 1 Requirements:

*Three Parts:*

1. Created relational database following the business rules
2. Generated required reports using SQL
3. Generated SQL statements to create a certain amount of automation



> Business Rules.


* An attorney is retained by (or assigned to) one or more clients, for each case. 
* A client has (or is assigned to) one or more attorneys for each case. 
* An attorney has one or more cases. 
* A client has one or more cases. 
* Each court has one or more judges adjudicating. 
* Each judge adjudicates upon exactly one court. 
* Each judge may preside over more than one case. 
* Each case that goes to court is presided over by exactly one judge. 
* A person can have more than one phone number. 

> Further notes

* Attorney data must include social security number, name, address, office phone, home phone, e-mail, start/end dates, dob, hourly rate, earsin practice, bar (may be more than one - multivalued), specialty (may be more than one - multivalued). 
* Client data must include social security number, name, address, phone, e-mail, dob. 
* Case data must include type, description, start/end dates. 
* Court data must include name, address, phone, e-mail, url. 
* Judge data must include same information as attorneys (except bar, specialty and hourly rate; instead, use salary). 
* Must track judge historical data�tenure at each court (i.e., start/end dates), and salaries. 
* Also, history will track which courts judges presided over, if they took a leave of absence, or retired. 
* All tables must have notes. 

#### Assignment Screenshots:

(For better view, click this [Link](dataset.txt "File that has all the data and tables in my database"))

*Screenshot of ERD*:

![Database ERD](img/erd.png)


*Screenshot of datasets*:

![Dataset part 1](img/data.png)
![Dataset part 2](img/data2.png)
![Dataset part 3](img/data3.png)
![Dataset part 4](img/data4.png)
![Dataset part 5](img/data5.png)
![Dataset part 6](img/data6.png)
![Dataset part 7](img/data7.png)
![Dataset part 8](img/phone.png)

#### Assignment files:

[Database ERD](p1_erd.mwb "ERD for my database")

[Database Creation SQL](p1_sql_solution.sql "SQL that made my database")

[Data query SQL](p1_sql_solution.sql "The SQL code that answers the questions included in the assignment")

[File with all data in database](dataset.txt)


