-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema yc19a
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `yc19a` ;

-- -----------------------------------------------------
-- Schema yc19a
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `yc19a` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `yc19a` ;

-- -----------------------------------------------------
-- Table `yc19a`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`person` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`person` (
  `per_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_ssn` BINARY(64) NULL,
  `per_salt` BINARY(64) NULL,
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_street` VARCHAR(30) NOT NULL,
  `per_city` VARCHAR(30) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT(9) NOT NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_type` ENUM('a', 'c', 'j') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE INDEX `per_ssn_UNIQUE` (`per_ssn` ASC) VISIBLE,
  UNIQUE INDEX `per_salt_UNIQUE` (`per_salt` ASC) VISIBLE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`attorney`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`attorney` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`attorney` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `aty_start_date` DATE NOT NULL,
  `aty_end_date` DATE NULL,
  `aty_hourly_rate` DECIMAL(5,2) NOT NULL,
  `aty_years_in_practice` TINYINT NOT NULL,
  `aty_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_attorney_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`bar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`bar` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`bar` (
  `bar_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `bar_name` VARCHAR(45) NOT NULL,
  `bar_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`bar_id`),
  INDEX `fk_bar_attorney1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_bar_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`phone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`phone` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`phone` (
  `phn_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `phn_num` BIGINT UNSIGNED NOT NULL,
  `phn_type` ENUM('c', 'w', 'h', 'f') NOT NULL,
  `phn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`phn_id`),
  INDEX `fk_phone_person1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_phone_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`specialty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`specialty` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`specialty` (
  `spc_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `spc_type` VARCHAR(45) NOT NULL,
  `spc_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`spc_id`),
  INDEX `fk_specialty_attorney1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_specialty_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`client` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`client` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `cli_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_client_person`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`court`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`court` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`court` (
  `crt_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `crt_name` VARCHAR(45) NOT NULL,
  `crt_street` VARCHAR(30) NOT NULL,
  `crt_city` VARCHAR(30) NOT NULL,
  `crt_state` CHAR(2) NOT NULL,
  `crt_zip` INT(9) NOT NULL,
  `crt_phone` BIGINT NOT NULL,
  `crt_email` VARCHAR(100) NOT NULL,
  `crt_url` VARCHAR(100) NOT NULL,
  `crt_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`crt_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`judge`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`judge` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`judge` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `crt_id` TINYINT UNSIGNED NULL,
  `jud_salary` DECIMAL(8,2) NOT NULL,
  `jud_years_in_practice` TINYINT NOT NULL,
  `jud_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  INDEX `fk_judge_court1_idx` (`crt_id` ASC) VISIBLE,
  CONSTRAINT `fk_judge_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_judge_court1`
    FOREIGN KEY (`crt_id`)
    REFERENCES `yc19a`.`court` (`crt_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`case` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`case` (
  `cse_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `cse_type` VARCHAR(45) NOT NULL,
  `cse_description` TEXT NOT NULL,
  `cse_start_date` DATE NOT NULL,
  `cse_end_date` DATE NULL,
  `cse_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cse_id`),
  INDEX `fk_case_judge1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_case_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`assignment` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`assignment` (
  `asn_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_cid` SMALLINT UNSIGNED NOT NULL,
  `per_aid` SMALLINT UNSIGNED NOT NULL,
  `cse_id` SMALLINT UNSIGNED NOT NULL,
  `asn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`asn_id`),
  INDEX `fk_assignment_client1_idx` (`per_cid` ASC) VISIBLE,
  INDEX `fk_assignment_attorney1_idx` (`per_aid` ASC) VISIBLE,
  INDEX `fk_assignment_case1_idx` (`cse_id` ASC) VISIBLE,
  CONSTRAINT `fk_assignment_client1`
    FOREIGN KEY (`per_cid`)
    REFERENCES `yc19a`.`client` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_assignment_attorney1`
    FOREIGN KEY (`per_aid`)
    REFERENCES `yc19a`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_assignment_case1`
    FOREIGN KEY (`cse_id`)
    REFERENCES `yc19a`.`case` (`cse_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `yc19a`.`judge_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `yc19a`.`judge_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `yc19a`.`judge_hist` (
  `jhs_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `jhs_crt_id` TINYINT UNSIGNED NULL,
  `jhs_date` TIMESTAMP NOT NULL,
  `jhs_type` ENUM('i', 'u', 'd') NOT NULL,
  `jhs_salary` DECIMAL(8,2) NOT NULL,
  `jhs_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`jhs_id`),
  INDEX `fk_judge_hist_judge1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_judge_hist_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `yc19a`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
USE `yc19a`;

DELIMITER $$

USE `yc19a`$$
DROP TRIGGER IF EXISTS `yc19a`.`trg_judge_after_insert` $$
SHOW WARNINGS$$
USE `yc19a`$$
create trigger trg_judge_after_insert
AFTER INSERT on judge
# after insert means that this trigger triggers after something is inserted on the employee table
FOR EACH ROW
BEGIN
INSERT into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
# The NEW. keyword is used to indicate the newly inserted values on employee table
# The OLD. Keyword is used to indicate the data that used to be there on the employee table
(NEW.per_id, NEW.crt_id, now(), 'i', NEW.jud_salary, NEW.jud_notes);
# The user() function gets the user that is using the database to make changes at the time of change
# The now() function gets the current datetime of the time of change

end$$

SHOW WARNINGS$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `yc19a`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rpchester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bruce ', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'c', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'c', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04', 'a', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland', 'OH', 022348890, 'hypm@aol.com', '1980-08-28', 'a', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird LN', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', '1980-08-22', 'j', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Adam', 'Jurris', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct', 'Billings', 'MT', 672048823, 'jsleen@sysmpatico.com', '1970-03-22', 'j', NULL);
INSERT INTO `yc19a`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`attorney`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (6, '2006-06-12', NULL, 85, 5, NULL);
INSERT INTO `yc19a`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (7, '2003-08-20', NULL, 130, 28, NULL);
INSERT INTO `yc19a`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (8, '2009-12-12', NULL, 70, 17, NULL);
INSERT INTO `yc19a`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (9, '2008-06-08', NULL, 78, 13, NULL);
INSERT INTO `yc19a`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`bar`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Florida bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Alabama bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Georgia bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'Michigan bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'South Carolina bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Montana bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Arizona Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Nevada Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'New York Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'New York Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Mississippi Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'California Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Illinois Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'Indiana Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'Illinois Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Tallahassee Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Ocala Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Bay County Bar', NULL);
INSERT INTO `yc19a`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'Cincinatti Bar', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`phone`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 1, 803228827, 'c', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 2, 2052338293, 'h', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 4, 1034325598, 'w', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 5, 6402338494, 'w', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 6, 5508329842, 'f', 'fax number currently not working');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 7, 8202052203, 'c', 'prefers home calls');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 8, 4008338294, 'h', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 9, 7654328912, 'w', NULL);
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 10, 5463721984, 'f', 'work fax number');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 11, 4537821902, 'h', 'prefers cell phone calls');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 12, 7867821902, 'w', 'best phone number to reach');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 13, 4537821654, 'w', 'call during lunch');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 14, 3721821902, 'c', 'prefers cell phone calls');
INSERT INTO `yc19a`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 15, 9217821945, 'f', 'use for faxing legal docs');

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`specialty`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 6, 'business', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 7, 'traffic', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 8, 'bankruptcy', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 9, 'insurance', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 10, 'judicial ', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 6, 'environmental', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 7, 'criminal', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 8, 'real estate', NULL);
INSERT INTO `yc19a`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 9, 'malpractice', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`client`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`client` (`per_id`, `cli_notes`) VALUES (1, NULL);
INSERT INTO `yc19a`.`client` (`per_id`, `cli_notes`) VALUES (2, NULL);
INSERT INTO `yc19a`.`client` (`per_id`, `cli_notes`) VALUES (3, NULL);
INSERT INTO `yc19a`.`client` (`per_id`, `cli_notes`) VALUES (4, NULL);
INSERT INTO `yc19a`.`client` (`per_id`, `cli_notes`) VALUES (5, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`court`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'leon country circuit court', '301 south monroe street', 'tallahassee', 'fl', 323035292, 8506065504, 'lccc@us.fl.gov', 'http://www.leoncountycircuitcourt.gov/', NULL);
INSERT INTO `yc19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'leon county traffic court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8505774100, 'lctc@us.fl.gov', 'http://www.leoncountytrafficcourt.gov/', NULL);
INSERT INTO `yc19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'florida supreme court', '500 south duval street', 'tallahassee', 'fl', 323035292, 8504880125, 'fsc@us.fl.gov', 'http://www.floridasupremecourt.org/', NULL);
INSERT INTO `yc19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'orange county courthouse', '424 north orange avenue', 'tallahassee', 'fl', 328012248, 4078362000, 'occ@us.fl.gov', 'http://www.ninthcircuit.org/', NULL);
INSERT INTO `yc19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'fifth district court of appeal', '300 south beach street', 'tallahassee', 'fl', 328012248, 3862258600, '5dca@us.fl.gov', 'http://www.5dca.org/', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`judge`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (11, 5, 150000, 10, NULL);
INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (12, 4, 185000, 1, NULL);
INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (13, 4, 135000, 3, NULL);
INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (14, 3, 170000, 2, NULL);
INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (15, 1, 120000, 6, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`case`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 13, 'civil', 'client says his logo is being used without his consent to promote a rival business', '2010-09-09', NULL, 'copyright infringement');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 12, 'criminal', 'client is charge with assaulting her husband during an argument', '2009-11-19', '2010-12-23', 'assault');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor has just been mopped', '2008-05-06', '2008-07-23', 'slip and fall');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. client has a solid alibi', '2011-05-20', NULL, 'grand theft');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 13, 'criminal', 'client charged with posession of 10 grams of cocaine, allegedly found his glove box by state police', '2011-06-05', NULL, 'posession of narcotics');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 14, 'civil', 'client alleges newspaper [rinted false information about his personal activities while he ran a large laundry business in a small nearby town', '2007-01-19', '2007-05-20', 'defamation');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 12, 'criminal', 'client charged with the murder of his co-worker over a lovers fued. client has no alilbi', '2010-03-20', NULL, 'murder');
INSERT INTO `yc19a`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and declared bankcruptcy', '2012-01-26', '2013-02-28', 'bankruptcy');

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`assignment`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 1, 6, 7, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 2, 6, 6, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 3, 7, 2, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 4, 8, 2, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 5, 9, 5, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 1, 10, 1, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 2, 6, 3, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 3, 7, 8, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 4, 8, 8, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 5, 9, 8, NULL);
INSERT INTO `yc19a`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 4, 10, 4, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `yc19a`.`judge_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `yc19a`;
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 11, 3, '2009-01-16', 'i', 130000, NULL);
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 12, 2, '2010-05-27', 'i', 140000, NULL);
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 13, 5, '2000-01-02', 'i', 115000, NULL);
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 13, 4, '2005-07-05', 'i', 135000, NULL);
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 14, 4, '2008-12-09', 'i', 155000, NULL);
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice');
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court');
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice');
INSERT INTO `yc19a`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to couret based upon local area population growth');

COMMIT;

-- generate, insert, and secure ssn into person table (we can't use real ssns, so we gotta generate them)

drop procedure if exists CreatePersonSSN;
Delimiter $$
create procedure CreatePersonSSN()
Begin

Declare x, y int; -- declare variables to keep while loop in check
set x = 1; -- initialize x with value 1

-- dynamically set loop ending value (total number of persons)
select count(*) into y from person; -- set y as the number of people in the persons table
-- can test using "select y" to see if the assignment worked

while x <= y DO

-- give each person a unique randomized salt, and hashed and salted randomized SSN.
-- Note: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes!
set @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
set @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive
set @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

-- add ssn and salt values into person table as securely as possible
update person 
set per_ssn=@ssn, per_salt=@salt
where per_id=x;
set x = x + 1;

End while;

END $$
Delimiter ;
call CreatePersonSSN();
