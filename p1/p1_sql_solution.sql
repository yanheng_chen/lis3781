use yc19a;

select * from attorney;

select * from judge;

select * from person;

select * from bar;

select * from specialty;

#1
drop view if exists aty_info;
create view aty_info as
select concat(per_fname, ", ", per_lname) as `full name`, 
concat(per_street, ', ', per_city, ', ', per_state, ', ', per_zip, ', ') as `address`,
timestampdiff(year,per_dob ,now()) as `age`,
aty_hourly_rate, spc_type, bar_name 
from person
natural join attorney
natural join bar
natural join specialty
order by per_lname asc;

select * from aty_info;


#2
drop procedure if exists judge_person_month;

delimiter //
create procedure judge_person_month()
begin
select per_dob, monthname(per_dob) as `month`, count(*) from person 
natural join judge
group by `month`, per_dob;
end//
delimiter ;

call judge_person_month();


#3
drop procedure if exists judge_case_info;

delimiter //
create procedure judge_case_info()
begin
select concat(per_fname, ", ", per_lname) as `full name`, 
concat(per_street, ', ', per_city, ', ', per_state, ', ', per_zip, ', ') as `address`,
phn_num as `phone`, jud_years_in_practice as `years in practice`, cse_type, cse_description
from person
natural join judge
natural join `case`
natural join phone;
end //
delimiter ;

call judge_case_info();

#4

delimiter //
drop trigger trg_judge_after_insert//
create trigger trg_judge_after_insert
AFTER INSERT on judge
# after insert means that this trigger triggers after something is inserted on the employee table
FOR EACH ROW
BEGIN
INSERT into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
# The NEW. keyword is used to indicate the newly inserted values on employee table
# The OLD. Keyword is used to indicate the data that used to be there on the employee table
(NEW.per_id, NEW.crt_id, now(), 'i', NEW.jud_salary, NEW.jud_notes);
# The user() function gets the user that is using the database to make changes at the time of change
# The now() function gets the current datetime of the time of change

end //
delimiter ;

#5

delimiter //
drop trigger trg_judge_after_insert//
create trigger trg_judge_after_insert
AFTER UPDATE on judge
# after insert means that this trigger triggers after something is inserted on the employee table
FOR EACH ROW
BEGIN
INSERT into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
# The NEW. keyword is used to indicate the newly inserted values on employee table
# The OLD. Keyword is used to indicate the data that used to be there on the employee table
(NEW.per_id, NEW.crt_id, now(), 'u', NEW.jud_salary, Concat("modifying user: ", user(), ", ",NEW.jud_notes));
# The user() function gets the user that is using the database to make changes at the time of change
# The now() function gets the current datetime of the time of change

end //
delimiter ;

#6
show variables like 'event_scheduler';

set global event_scheduler = on;
drop event if exists one_time_add_judge;
CREATE EVENT one_time_add_judge
    ON SCHEDULE AT now() + INTERVAL 10 second
    comment 'adds a new judge only one time'
    DO
		INSERT INTO `yc19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (12, 1, 160000, 6, NULL);
        
delimiter ;
        




show warnings;

