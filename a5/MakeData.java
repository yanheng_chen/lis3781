import java.util.Random;
import java.io.FileWriter; //import file class
import java.io.IOException; //class IOException class to handle errors
import java.util.Scanner;
import java.time.LocalDate;

public class MakeData {
    public static void main(String[] args) {

        String type = insertType().toLowerCase();
        if (type.equals("sale")) {

            insertSales();
        } else if (type.equals("phone")) {

            insertPhone();
        } else if (type.equals("contact")) {
            insertContact();
        } else if (type.equals("order")) {
            insertOrder();
        } else if (type.equals("invoice")) {
            insertInvoice();
        } else {
            System.out.println("this is not an option");
        }
    }

    public static void insertSales() {
        int wanted = insertNum();
        try {
            FileWriter myWriter = new FileWriter("insertsales.sql", true);
            for (int i = 0; i < wanted; i++) {
                Random rand = new Random();

                if (i == wanted)
                    myWriter.write(
                            "Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(5)) + ", "
                                    + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(5)) + ", "
                                    + (10 + rand.nextInt(1000)) + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9)) + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9))
                                    + ", NULL);\n");
                else
                    myWriter.write(
                            "Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(5)) + ", "
                                    + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(5)) + ", "
                                    + (10 + rand.nextInt(1000)) + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9)) + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9))
                                    + ", NULL);\n");

            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    public static void insertPhone() {
        int wanted = insertNum(); // pass the insertNum value into wanted
        try {
            Random rand = new Random();
            FileWriter myWriter = new FileWriter("insertphones.sql", true);
            for (int i = 0; i < wanted; i++) {

                if (i == wanted)
                    myWriter.write(
                            "Insert into dbo.phone (per_id, phn_num, phn_type,phn_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(15)) + ", " + (long) (Math.random() * Math.pow(10, 10))
                                    + ", " + "\'"
                                    + phn_type.getPhnType() + "\'"
                                    + " , NULL);\n");
                else
                    myWriter.write(
                            "Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(15)) + ", " + (long) (Math.random() * Math.pow(10, 10))
                                    + ", " + "\'"
                                    + phn_type.getPhnType() + "\'"
                                    + " , NULL);\n");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    public static void insertContact() {
        int wanted = insertNum(); // pass the insertNum value into wanted

        try {
            Random rand = new Random();
            FileWriter myWriter = new FileWriter("insertcontacts.sql", true);
            for (int i = 0; i < wanted; i++) {
                LocalDate randomDate = createRandomDate(1980, 2022);
                if (i == wanted)
                    myWriter.write(
                            "Insert into dbo.contact (per_cid, per_sid, cnt_date, cnt_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(15))
                                    + ", " +
                                    randomDate
                                    + " , NULL);\n");
                else
                    myWriter.write(
                            "Insert into dbo.contact (per_cid, per_sid, cnt_date, cnt_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(5)) + ", " + (1 + rand.nextInt(15))
                                    + ", " +
                                    randomDate
                                    + " , NULL);\n");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    public static void insertOrder() {
        int wanted = insertNum(); // pass the insertNum value into wanted

        try {
            Random rand = new Random();
            FileWriter myWriter = new FileWriter("insertorders.sql", true);
            for (int i = 0; i < wanted; i++) {
                LocalDate randomDate = createRandomDate(1980, 2022);
                if (i == wanted)
                    myWriter.write(
                            "Insert into dbo.[order] (cnt_id, ord_placed_date, ord_filled_date, ord_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(10)) + ", " + randomDate + ", " + " null"
                                    + " , NULL);\n");
                else
                    myWriter.write(
                            "Insert into dbo.[order] (cnt_id, ord_placed_date, ord_filled_date, ord_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(10)) + ", " + randomDate + ", " + " null"
                                    + " , NULL);\n");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    public static void insertInvoice() {
        int wanted = insertNum(); // pass the insertNum value into wanted

        try {
            Random rand = new Random();
            FileWriter myWriter = new FileWriter("insertinvoice.sql", true);
            for (int i = 0; i < wanted; i++) {
                LocalDate randomDate = createRandomDate(1980, 2022);
                if (i == wanted)
                    myWriter.write(
                            "insert into dbo.invoice (ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(10)) + ", " + (1 + rand.nextInt(5)) + ", " + randomDate
                                    + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9)) + ", " +
                                    (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9))
                                    + " , NULL);\n");
                else
                    myWriter.write(
                            "insert into dbo.invoice (ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes) values "
                                    +
                                    "(" + (1 + rand.nextInt(10)) + ", " + (1 + rand.nextInt(5)) + ", " + randomDate
                                    + ", "
                                    + (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9)) + ", " +
                                    (rand.nextInt(100000)) + "." + (rand.nextInt(9)) + (rand.nextInt(9))
                                    + " , NULL);\n");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    // ask what table you want the inserts for
    public static String insertType() {
        Scanner sc = new Scanner(System.in);
        System.out.println(
                "What do you want to insert for Jowett? Options include phone, sale, contact, order, and invoice");
        String type = " ";
        while (!sc.hasNext()) {
            System.out.println("Not valid String");
            sc.next(); // makes user input another value
            System.out.println("Please try again. Enter strings");
        }
        type = sc.next();

        return type;
    }

    // asks how many records you want inserts for
    public static int insertNum() {
        Scanner scnr = new Scanner(System.in);
        System.out.println("How many inserts do you want?");
        int num1 = 0;
        while (!scnr.hasNextInt()) {
            System.out.println("Not a valid integer.");
            // if omitted, the user will never get their chance to input their value
            System.out.println("Please try again. Enter number: ");
            scnr.next();
        }
        num1 = scnr.nextInt();
        scnr.close();

        return num1;
    }

    private enum phn_type {
        f,
        w,
        c,
        h;

        /**
         * Pick a random value of the phn_type enum.
         *
         * @return a random phn_type.
         */
        public static phn_type getPhnType() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }

    public static int createRandomIntBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static LocalDate createRandomDate(int startYear, int endYear) {
        int day = createRandomIntBetween(1, 28);
        int month = createRandomIntBetween(1, 12);
        int year = createRandomIntBetween(startYear, endYear);
        return LocalDate.of(year, month, day);
    }

}
