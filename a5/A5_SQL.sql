set ansi_warnings ON;

use master;
GO

--select * from dbo.person;

if exists (select name from master.dbo.sysdatabases where name = N'yc19a')
drop database yc19a;
Go

if not exists (select name from master.dbo.sysdatabases where name = N'yc19a')
create database yc19a;
GO

use yc19a;
go

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'CreatePersonSSN') AND type in (N'P', N'PC'))
  DROP PROCEDURE dbo.CreatePersonSSN
GO
create procedure dbo.CreatePersonSSN
as
begin

declare @salt binary(64);
declare @ran_num int;
declare @ssn binary(64);
declare @x int, @y int;

set @x=1;

set @y = (select count(*) from dbo.person);

while (@x < @y)
begin
set @salt=CRYPT_GEN_RANDOM(64);
set @ran_num=FLOOR(RAND()*(999999999 - 111111111 + 1)) + 111111111;
set @ssn=HASHBYTES('sha2_512', concat(@salt, @ran_num));

update dbo.person
set per_ssn=@ssn, per_salt=@salt
where per_id = @x;

set @x = @x + 1;
end;
end;
GO


if object_id(N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person
(
per_id smallint not null identity(1,1),
per_ssn binary(64) null,
per_salt binary(64) null,
per_fname varchar(15) not null,
per_lname varchar(30) not null,
per_gender char(1) not null check (per_gender in ('m', 'f')),
per_dob date not null,
per_street varchar(30) not null,
per_city varchar(30) not null,
per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
per_state char(2) not null default 'FL',
per_email varchar(100) null,
per_type char(1) not null check (per_type in('c', 's')),
per_notes varchar(45) null,
primary key(per_id),

-- SSN and state IDs are unique with the constraint below
constraint ux_per_ssn unique nonclustered (per_ssn asc) );
go

-- select * from dbo.person;

if object_id(N'dbo.phone', N'U') is not null
drop table dbo.phone;
go

create table dbo.phone
(
phn_id smallint not null identity(1,1),
per_id smallint not null,
phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
phn_type char(1) not null check (phn_type in('h','c','w','f')),
phn_notes varchar(255) null,
primary key (phn_id),

constraint fk_phone_person
foreign key (per_id)
references dbo.person(per_id)
on delete cascade
on update cascade
);

-- select * from dbo.phone;

if object_id(N'dbo.customer', N'U') is not null
drop table dbo.customer;
go

create table dbo.customer
(
per_id smallint not null,
cus_balance decimal(7,2) not null check (cus_balance >= 0),
cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
cus_notes varchar(45) null,
primary key (per_id),

constraint fk_customer_person
foreign key (per_id)
references dbo.person(per_id)
on delete cascade
on update cascade
);

-- select * from dbo.customer

if object_id(N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
go

create table dbo.slsrep
(
per_id smallint not null,
srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
srp_ytd_sales decimal (8,2) not null check (srp_ytd_sales >- 0),
srp_ytd_comm decimal (7,2) not null check (srp_ytd_comm >= 0),
srp_notes varchar(45) null,
primary key(per_id),

constraint fk_slsrep_person
foreign key (per_id)
references dbo.person(per_id)
on delete cascade
on update cascade
);

-- select * from slsrep;


if object_id(N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
go

create table dbo.srp_hist
(
sht_id smallint not null identity (1,1),
per_id smallint not null,
sht_type char(1) not null check (sht_type in ('i', 'u', 'd')),
sht_modified datetime not null,
sht_modifier varchar(45) not null default system_user,
sht_date date not null default getDate(),
sht_yr_sales_goal decimal (8,2) not null check (sht_yr_sales_goal >= 0),
sht_yr_total_sales decimal (8,2) not null check (sht_yr_total_sales >= 0),
sht_yr_total_comm decimal (7,2) not null check (sht_yr_total_comm >= 0),
sht_notes varchar(45) null,
primary key (sht_id),

constraint fk_srp_hist_slsrep
foreign key (per_id) 
references dbo.slsrep(per_id)
on delete cascade
on update cascade
);

-- select * from dbo.srp_hist


if object_id(N'dbo.contact', N'U') is not null
drop table dbo.contact;
go

create table dbo.contact
(
cnt_id int not null identity(1,1),
per_cid smallint not null,
per_sid smallint not null,
cnt_date datetime not null,
cnt_notes varchar(255) null,
primary key (cnt_id),

constraint fk_contact_customer
foreign key (per_cid)
references dbo.customer(per_id)
on delete cascade
on update cascade,

constraint fk_contact_slsrep
foreign key (per_sid)
references dbo.slsrep(per_id)
on delete no action 
on update no action
);

-- select * from dbo.contact;


if object_id(N'dbo.order', N'U') is not null
drop table dbo.[order];
go

create table dbo.[order]
(
ord_id int not null identity(1,1),
cnt_id int not null,
ord_placed_date datetime not null,
ord_filled_date datetime null,
ord_notes varchar(255) null,
primary key (ord_id),

constraint fk_order_contact
foreign key (cnt_id)
references dbo.contact(cnt_id)
on delete cascade
on update cascade
);

--select * from dbo.[order];
if object_id (N'dbo.region', N'U') is not null
drop table dbo.region;
Go

create table dbo.region
(
reg_id tinyint not null identity (1,1),
reg_name char(1) not null,
reg_notes varchar(255) null,
primary key (reg_id)
);
go

if object_id (N'dbo.state', N'U') is not null
drop table dbo.state;
Go

create table dbo.state
(
ste_id tinyint not null identity (1,1),
reg_id tinyint not null,
ste_name char(2) not null,
ste_notes varchar(255) null,
primary key (ste_id),

constraint fk_state_region
foreign key (reg_id)
references dbo.region (reg_id)
on delete cascade
on update cascade
);
go

if object_id (N'dbo.city', N'U') is not null
drop table dbo.city;
Go

create table dbo.city
(
cty_id smallint not null identity (1,1),
ste_id tinyint not null,
cty_name varchar(30) not null,
cty_notes varchar(255) null,
primary key (cty_id),

constraint fk_city_state
foreign key (ste_id)
references dbo.state (ste_id)
on delete cascade
on update cascade
);
go

if object_id(N'dbo.store', N'U') is not null
drop table dbo.store;
go

create table dbo.store
(
str_id smallint not null identity(1,1),
cty_id smallint not null,
str_name varchar(45) not null,
str_street varchar(30) not null,
str_city varchar(40) not null,
str_state char(2) not null default 'FL',
str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_email varchar(100) not null,
str_url varchar(100) not null,
str_notes char(255) null,
primary key (str_id),

constraint fk_store_city
foreign key (cty_id)
references dbo.city (cty_id)
on delete cascade
on update cascade
);

--select * from dbo.store;

if object_id(N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
go



create table dbo.invoice
(
inv_id int not null identity(1,1),
ord_id int not null,
str_id smallint not null,
inv_date datetime not null,
inv_total decimal (8,2) not null check (inv_total >= 0),
inv_paid bit not null,
inv_notes varchar(255) null,
primary key (inv_id),

--create one to one relationship with order table by making ord_id unique
constraint ux_ord_id unique nonclustered (ord_id asc),

constraint fk_invoice_order
foreign key (ord_id)
references dbo.[order] (ord_id)
on delete cascade 
on update cascade,

constraint fk_invoice_store
foreign key (str_id)
references dbo.store (str_id)
on delete cascade
on update cascade
);

-- select * from dbo.invoice;

if object_id(N'dbo.payment', N'U') is not null
drop table dbo.payment;
go

create table dbo.payment
(
pay_id int not null identity (1,1),
inv_id int not null,
pay_date datetime not null,
pay_amt decimal(7,2) not null check (pay_amt >= 0),
pay_notes varchar(255) null,
primary key (pay_id),

constraint fk_payment_invoice
foreign key (inv_id)
references dbo.invoice (inv_id)
on delete cascade
on update cascade
);

-- select * from dbo.payment;

if object_id(N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
go

create table dbo.vendor
(
ven_id smallint not null identity(1,1),
ven_name varchar(45) not null,
ven_street varchar(30) not null,
ven_city varchar(30) not null,
ven_state char(2) not null default 'FL',
ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_email varchar(100) null,
ven_url varchar(100) null,
ven_notes varchar(255) null,
primary key(ven_id),
);

-- select * from dbo.vendor;

if object_id(N'dbo.product', N'U') is not null
drop table dbo.product;
go

create table dbo.product
(
pro_id smallint not null identity(1,1),
ven_id smallint not null,
pro_name varchar(30) not null,
pro_descript varchar(45) null,
pro_weight float not null check (pro_weight >= 0),
pro_qoh smallint not null check (pro_qoh >= 0),
pro_cost smallint not null check (pro_cost >= 0),
pro_price decimal (7,2) not null check (pro_price >= 0),
pro_discount decimal (3,0) null,
pro_notes varchar(255) null,
primary key (pro_id),

constraint fk_product_vendor
foreign key (ven_id)
references dbo.vendor(ven_id)
on delete cascade
on update cascade
);

--select * from product;

if object_id(N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
go

create table dbo.product_hist
(
pht_id int not null identity(1,1),
pro_id smallint not null,
pht_date datetime not null,
pht_cost decimal(7,2) not null check (pht_cost >= 0),
pht_price decimal(7,2) not null check (pht_price >= 0),
pht_discount decimal (3,0) null,
pht_notes varchar(255) null,
primary key (pht_id),

constraint fk_product_hist_product 
foreign key (pro_id)
references dbo.product (pro_id)
on delete cascade
on update cascade
);

-- select * from dbo.product_hist;

if object_id(N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
go

create table dbo.order_line
(
oln_id int not null identity (1,1),
ord_id int not null,
pro_id smallint not null,
oln_qty smallint not null check (oln_qty >= 0),
oln_price decimal(7,2) not null check (oln_price >= 0),
oln_notes varchar(255) null,
primary key (oln_id),

-- must use delimter [] on reserved words(like order)
constraint fk_order_line_order
foreign key (ord_id)
references dbo.[order] (ord_id)
on delete cascade
on update cascade,

constraint fk_order_line_product
foreign key (pro_id)
references dbo.product (pro_id)
on delete cascade 
on update cascade
);

if object_id (N'dbo.time', N'U') is not null
drop table dbo.time;
Go

create table dbo.time
(
tim_id int not null identity (1,1),
tim_yr smallint not null,
tim_qtr tinyint not null,
tim_month tinyint not null,
tim_week tinyint not null,
tim_day tinyint not null,
tim_time time not null,
tim_notes varchar(255) null,
primary key (tim_id) 
);
GO







if object_id (N'dbo.sale', N'U') is not null
drop table dbo.sale;
Go

create table dbo.sale(
pro_id smallint not null,
str_id smallint not null,
cnt_id int not null,
tim_id int not null,
sal_qty smallint not null,
sal_price decimal(8,2) not null,
sal_total decimal(8,2) not null,
sal_notes varchar(255) null,
primary key (pro_id, cnt_id, tim_id, str_id),

constraint ux_pro_id_str_id_cnt_id_tim_id
unique nonclustered (pro_id asc, str_id asc, cnt_id asc, tim_id asc),

constraint fk_sale_time
foreign key (tim_id)
references dbo.time (tim_id)
on delete cascade
on update cascade,

constraint fk_sale_contact
foreign key (cnt_id)
references dbo.contact (cnt_id)
on delete cascade 
on update cascade,

constraint fk_sale_store
foreign key (str_id)
references dbo.store (str_id)
on delete cascade
on update cascade,

constraint fk_sale_product
foreign key (pro_id)
references dbo.product (pro_id)
on delete cascade
on update cascade
);
go


select * from INFORMATION_SCHEMA.tables;
-- select * from dbo.order_line;

-- select * from INFORMATION_SCHEMA.tables;

-- populate tables

insert into dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
values
(1,null,'Steve', 'Rogers','m','1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net','s',null),
(2,null,'Bruce', 'Wayne','m','1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 983208440, 'bwayne@knology.net','s',null),
(3,null,'Peter', 'Parker','m','1988-09-12', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com','s',null),
(4,null,'Jane', 'Thompson','f','1978-05-08', '13563 Ocean View Street', 'Seattle', 'WA', 132084409, 'jthompson@gmail.com','s',null),
(5,null,'Debra', 'Steele','f','1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net','s',null),
(6,null,'Tony', 'Smith','m','1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com','c',null),
(7,null,'Hank', 'Pymi','m','1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 822348890, 'hpym@aol.com','c',null),
(8,null,'Bob', 'Best','m','1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com','c',null),
(9,null,'Sandra', 'Smith','f','1990-01-26', '87612 Lawrence Ave', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com','c',null),
(10,null,'Ben', 'Avery','m','1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com','c',null),
(11,null,'Arthur', 'Curry','m','1975-12-15', '3304 Eucild Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com','c',null),
(12,null,'Diana', 'Price','f','1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@sympatico.com','c',null),
(13,null,'Adam', 'Smith','m','1995-01-31', '98435 Valencia Dr.', 'Golf Shores', 'AL', 870219932, 'ajurris@gmx.com','c',null),
(14,null,'Judy', 'Sleen','f','1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@sympatico.com','c',null),
(15,null,'Bill', 'Neiderheim','m','1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net','c',null);


-- populate person table with salt and hashed values using CreatePersonSSN procedure created


exec dbo.CreatePersonSSN;

--select * from dbo.person;

insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(1, 100000, 60000, 1800, null),
(2, 80000, 35000, 3500, null),
(3, 150000, 84000, 9650, 'Great salesperson'),
(4, 125000, 87000, 15300, null),
(5, 98000, 43000, 8750, null);

-- select * from dbo.slsrep;

insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
values
(6, 120, 14789, null),
(7, 98.46, 234.92, null),
(8, 0, 4578, 'Customer always pays on time'),
(9, 981.73, 1672.38, 'High balance'),
(10, 541.23, 782.57, null),
(11, 251.02, 13782.95, 'Good customer'),
(12, 582.67, 963.12, 'Previously paid in full'),
(13, 121.67, 1057.45, 'Recent customer'),
(14, 765.43, 6789.42, 'Buys bult quantities'),
(15, 304.39, 456.81, 'Has not purchased recently');

-- Select * from dbo.customer;

insert into dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
values
(1,6,'1999-01-01',null),
(2,6,'2001-09-29',null),
(3,7,'2002-08-15',null),
(2,7,'2002-09-01',null),
(4,7,'2004-01-05',null),
(5,8,'2004-02-28',null),
(4,8,'2004-03-03',null),
(1,9,'2004-04-07',null),
(5,9,'2004-07-29',null),
(3,11,'2005-05-02',null),
(4,13,'2005-06-14',null),
(2,15,'2005-07-02',null);

-- select * from contact;

insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
values
(1, '2010-11-23', '2010-12-24', null),
(2, '2005-03-29', '2005-07-28', null),
(3, '2011-07-01', '2010-07-06', null),
(4, '2009-12-24', '2010-01-05', null),
(5, '2008-09-21', '2008-11-26', null),
(6, '2009-04-17', '2009-04-30', null),
(7, '2010-05-31', '2010-06-07', null),
(8, '2007-09-02', '2007-09-16', null),
(9, '2011-12-08', '2011-12-23', null),
(10, '2012-02-29', '2012-05-02', null);

-- select * from dbo.[order];

--insert into dbo.store
--(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
--values
--('Walgreens', '14567 Walnut Ln', 'Aspen', 'IL','475315690', '3127658127', 'info@walgreens.com', 'http://www.walgreens.com', null),
--('CVS', '572 Casper Rd', 'Chicago', 'IL','505261519', '3128926534', 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger'),
--('Lowes', '81309 Catapult Ave', 'CLover', 'WA','802345671', '9017653421', 'sales@lowes.com', 'http://www.lowes.com', null),
--('Walmart', '14567 Walnut Ln', 'St. Louis', 'FL','387563628', '8722718923', 'info@walmart.com', 'http://www.walmart.com', null),
--('Dollar General', '47583 Davision Rd', 'Detroit', 'MI','482983456', '3137583492', 'ask@dollargeneral.com', 'http://www.dollargeneral.com', 'recently sold property');
-- select * from dbo.store;



-- select * from dbo.invoice;

insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
values
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7642128543', 'sales@sysco.com', 'http://www.sysco.com', null),
('General Electric', '100 Happy Trails Dr.', 'Boston', 'MA', '213456874', '2134556941', 'support@ge.com', 'http://www.ge.com', 'very good turnaround'),
('Cisco', '300 Cisco Dr. ', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', null),
('Goodyear', '100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'competing well with Firestone.'),
('Snap-on', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!');

-- select * from dbo.vendor;

insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(1, 'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, null, null),
(3, 'pall', '16 Gallon', 2.8, 48, 3.89, 7.99, 40, null),
(4, 'cooking oil', 'Peanut Oil', 15, 19, 19.99, 28.99, null, 'gallons'),
(5, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale.');

-- select * from dbo.product;

insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
values
(1, 2, 10, 8.0, null),
(2, 3, 7, 9.88, null),
(3, 4, 3, 6.99, null),
(5, 1, 2, 12.76, null),
(4, 5, 13, 58.99, null);

-- select * from dbo.order_line



-- select * from dbo.payment;

insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
values
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, null, null),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, null),
(4, '2006-05-06 18:09:04', 19.99, 28.99, null, 'gallons'),
(5, '2006-04-07 15:04:29', 8.45, 13.99, 50, 'Currently 1/2 price sale');

-- select * from dbo.product_hist;

insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
values
(1, 'i', GETDATE(), SYSTEM_USER, getDate(), 100000, 110000, 11000, null),
(4, 'i', GETDATE(), SYSTEM_USER, getDate(), 150000, 175000, 17500, null),
(3, 'u', GETDATE(), SYSTEM_USER, getDate(), 200000, 185000, 18500, null),
(2, 'u', GETDATE(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, null),
(5, 'i', GETDATE(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, null);

INSERT INTO region 
(reg_name, reg_notes)
 VALUES 
 ('c',NULL), 
 ('n', NULL), 
 ('e', NULL), 
 ('s', NULL),
 ('w',NULL); 
 GO

select * from dbo.region;



INSERT INTO state 
(reg_id, ste_name, ste_notes) 
VALUES (1, 'MI', NULL),
(3, 'IL', NULL), 
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL);
Go

select * from dbo.state;

INSERT INTO city
(ste_id, cty_name, cty_notes) 
VALUES (1, 'Detroit', NULL), 
(2, 'Aspen', NULL), 
(2, 'Chicago', NULL), 
(3, 'Clover', NULL), 
(4, 'St. Louis',NULL);
GO



select * from dbo.city;

INSERT INTO dbo.store 
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES 
(2, 'Walgreens', '14567 Walnut Ln', 'Aspen', 'IL', 475315690, 3127658127, 'info@walgreens.com','http://www.walgreens.com', NULL), 
(3, 'CVS', '572 Casper Rd','Chicago', 'IL', 505231519,  3128926534, 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger.'), 
(4, 'Lowes', '81309 Catapult Ave', 'Clover', 'WA', 802345671, 9017653421, 'sales@lowes.com', 'http://www.lowes.com', NULL), 
(5, 'Walmart', '14567 Walnut Ln', 'St. Louis', 'FL', 387563628, 8722718923, 'infowalmart.com', 'http://www.walmart.com', NULL),
(1, 'Dollar General', '147583 Davison Rd', 'Detroit', 'MI', 482983456, 3137583492, 'ask@dollargeneral.com', 'http://www.dollargerieral.com', 'recently sold property');
GO

INSERT INTO time 
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes) 
VALUES 
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL), 
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL),
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL), 
(2014, 1, 2, 8, 2, '12:22:14', NULL),
(2013, 3, 9, 38, 4, '10:12:28', NULL), 
(2012, 4, 11, 47, 3, '22:36:22', NULL), 
(2014, 2, 6, 23, 3, '19:07:10', NULL); 
GO

select * from dbo.time;

insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
values
(5, 1, '2001-05-03', 58.32, 0, null),
(4, 1, '2006-11-11', 100.59, 0, null),
(1, 1, '2010-09-16', 57.34, 0, null),
(3, 2, '2011-01-10', 99.32, 1, null),
(2, 3, '2008-06-24', 1109.67, 1, null),
(6, 4, '2009-04-20', 239.83, 0, null),
(7, 5, '2010-06-05', 537.29, 0, null),
(8, 2, '2007-09-09', 644.21, 1, null),
(9, 3, '2011-12-17', 934.12, 1, null),
(10, 4, '2012-03-18', 27.45, 0, null);

insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
values
(5, '2008-07-01', 5.99, null),
(4, '2010-09-28', 4.99, null),
(1, '2008-07-23', 8.75, null),
(3, '2010-10-31', 19.55, null),
(2, '2011-03-29', 32.5, null),
(6, '2010-10-03', 20.00, null),
(8, '2008-08-09', 1000.00, null),
(9, '2009-01-10', 103.68, null),
(7, '2007-03-15', 25.00, null),
(10, '2007-05-12', 40.00, null),
(4, '2007-05-22', 9.33, null),
(5, '2008-07-01', 5.99, null);

INSERT INTO sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) 
VALUES
(1, 5, 5, 3, 20, 9.99, 199.8, NULL), 
(2, 4, 6, 2, 5, 5.99, 29.95, NULL),
(3, 3, 4, 1, 30, 3.99, 119.7, NULL), 
(4, 2, 1, 5, 15, 18.99, 284.85, NULL), 
(5, 1, 2, 4, 6, 11.99, 71.94, NULL), 
(5, 2, 5, 6, 10, 9.99, 199.8, NULL), 
(4, 3, 6, 7, 5, 8.99, 29.95, NULL), 
(3, 1, 4, 8, 30, 3.99, 119.7, NULL),
(2, 3, 1, 9, 15, 18.99, 284.85, NULL), 
(1, 4, 2, 10, 6, 11.99, 71.94, NULL), 
(1, 2, 3, 11, 10, 11.99, 119.9, NULL),
(4, 2, 5, 3, 20, 3.99, 99.8, NULL), 
(2, 5, 9, 2, 5, 5.99, 25.95, NULL),
(3, 5, 1, 1, 30, 34.99, 19.7, NULL), 
(4, 5, 1, 3, 15, 17.99, 284.85, NULL), 
(5, 2, 2, 9, 6, 11.79, 71.94, NULL), 
(1, 2, 5, 6, 10, 39.99, 199.8, NULL), 
(4, 3, 5, 6, 5, 95.99, 29.95, NULL), 
(1, 3, 4, 8, 30, 233.99, 119.7, NULL),
(1, 3, 1, 9, 15, 128.99, 284.85, NULL), 
(3, 4, 2, 10, 6, 112.99, 71.94, NULL), 
(4, 2, 3, 11, 10, 111.99, 119.9, NULL),
(5, 1, 4, 8, 30, 34.99, 119.7, NULL),
(3, 3, 1, 9, 15, 185.99, 284.85, NULL), 
(2, 4, 2, 10, 6, 110.99, 71.94, NULL), 
(3, 2, 3, 11, 10, 1163.99, 119.9, NULL);
GO

select * from dbo.sale;

Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (12, 4688242124, 'c' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (11, 7087853178, 'c' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (12, 2830010666, 'w' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (14, 7630568018, 'w' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (4, 7987207370, 'f' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (8, 2321464050, 'w' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (12, 5188623735, 'f' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (1, 7894484889, 'w' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (12, 5764083857, 'f' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (15, 8025482661, 'c' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (1, 3176264662, 'c' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (1, 9256605085, 'w' , NULL);
Insert into dbo.phone (per_id, phn_num, phn_type, phn_notes) values (11, 4226663779, 'f' , NULL);
select * from dbo.phone;

--select * from dbo.srp_hist;

--select year(sht_date) from dbo.srp_hist;

select * from contact;
select * from customer;
select * from invoice;
select * from [order];
select * from payment;
select * from person;
select * from phone;
select * from product;
select * from slsrep;
select * from srp_hist;
select * from store;
select * from vendor;
select * from product_hist;